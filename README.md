
# FSU Votes Website

## Shannon McWaters

### Repo contains:

1. [Ready to register block](register/README.md "Ready to register three coloumn block")
    - Create three column block
    - Create interactive buttons
    - Add three photos to each column
2. [Find your on-campus address page](on-campus/README.md "On-campus address page")
    - Create simple page with various styled text
3. [FAQ page](FAQ/README.md "FAQ page")
    - Create FAQ page with accordian answers
    - Add links to respective informational pages
4. [Ballot measures block](ballot/README.md "Ballot measures div block")
    - Create six div blocks with shadow block style
    - Create interactive buttons
    - Create modals with more ballot information to interact with button click
5. [Voting site page](voting/README.md "Voting site page")
    - Create a page with nested unordered list
    - Create hyperlinks to relevant pages
